turtle
======

Description
-----------

Submission for assignment 2 of of COMP6361: Principles of Programming
Languages; completed in semester 2, 2013 at Australian National University.

Build with:
``bash
make
``

Run all tests with:
``bash
for f in tests/*.t
do
  make ${f%.t}.p
done
``

Participants
------------

* Andrew Haigh (u4667010)
* Joshua Nelson (u4850020)

