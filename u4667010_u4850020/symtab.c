/*
 * symtab.c
 *
 * Header file for the symbol table
 * Modified from the bison & flex lab code
 *
 * COMP6361 assignment 2
 * u4667010
 * u4850020
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include "symtab.h"

//---Variable symbol table---
//Put a variable called 'ident', with address of 'address', into the symbol
//table 'table'
variable_symrec* put_variable_symbol(char* ident, short location, symbol_type type, variable_symrec** table) {
    variable_symrec *ptr;
    ptr = (variable_symrec *) malloc (sizeof (variable_symrec));
    ptr->name = (char *) malloc (strlen (ident) +1);
    strcpy (ptr->name, ident);
    ptr->address = location;
    ptr->type = type;
    ptr->next = (struct variable_symrec *) *table;
    *table = ptr;
    return ptr;
}

//Returns the whole symbol record from the given table
variable_symrec* get_variable_symbol(char* ident, variable_symrec** table){
    variable_symrec *ptr;
    for (ptr = *table; ptr != (variable_symrec *) 0; ptr = (variable_symrec *)ptr->next){
        if (strcmp (ptr->name,ident) == 0){
            return ptr;
        }
    }
    return 0;
}

//When exiting a function, we want to delete all local entries on the top of
//the table
variable_symrec* purge_local_variables(variable_symrec** table){
    variable_symrec *ptr = *table;
    while((ptr != (variable_symrec *) 0) && (ptr->type == LOCAL)){
        //fprintf(stderr,"About to delete %s\n",ptr->name);
        variable_symrec* next_record = (variable_symrec *)ptr->next;
        free(ptr->name); //we malloc'd some memory for the name
        free(ptr);
        ptr = next_record;

        if(next_record == (variable_symrec *) 0){
            break;
        }

    }
    *table = ptr;

    return 0;
}

//When done parsing parameters, we subtract the parameter count from all of
//them to get the address relative to FP
variable_symrec* offset_local_variables(variable_symrec** table, int offset){
    variable_symrec *ptr = *table;
    while((ptr != (variable_symrec *) 0) && (ptr->type == LOCAL)){
        ptr->address = (ptr->address) + offset;
        ptr = ptr->next;
    }

    return 0;
}

//Print the variable symbol table to stderr
void print_variable_symbol_table(variable_symrec** table){
    variable_symrec *ptr;
    if(*table==NULL){
        fprintf(stderr,"===Empty variable table===\n");
        return;
    }
    fprintf(stderr,"===Symbol table===\n");
    for (ptr = *table; ptr != (variable_symrec *)0; ptr = (variable_symrec *)ptr->next){
        char* type_string = "";
        switch(ptr->type){
            case GLOBAL:
                type_string = "global"; break;
            case LOCAL:
                type_string = "local"; break;
        }
        fprintf(stderr, "%s \t-> %d (%s)\n",(ptr)->name,(ptr)->address, type_string);
    }
}




//---Function symbol table---


//Keep track of yet-to-be-defined functions
undefined_function_symrec* put_undefined_function(char* ident, short location,int actual_line_number, int number_of_parameters,undefined_function_symrec** table){
    undefined_function_symrec *ptr;
    ptr = (undefined_function_symrec *) malloc (sizeof(undefined_function_symrec));
    ptr->name = (char *) malloc (strlen(ident) + 1);
    strcpy(ptr->name, ident);
    ptr->address = location;
    ptr->number_of_parameters = number_of_parameters;
    ptr->actual_line_number = actual_line_number;
    ptr->has_been_patched = 0;
    ptr->next = (struct undefined_function_symrec *) *table;
    *table = ptr;
    return ptr;
}

//Put a function called 'ident', with address of 'address', into the symbol
//table 'table'
function_symrec* put_function_symbol(char* ident, int parameter_count, short location, function_symrec** table) {
    function_symrec *ptr;
    ptr = (function_symrec *) malloc (sizeof (function_symrec));
    ptr->name = (char *) malloc (strlen (ident) +1);
    strcpy (ptr->name, ident);
    ptr->address = location;
    ptr->number_of_parameters = parameter_count;
    ptr->next = (struct function_symrec *) *table;
    *table = ptr;
    return ptr;
}

//Returns the whole symbol record from the given table
function_symrec* get_function_symbol(char* ident, function_symrec** table){
    function_symrec *ptr;
    for (ptr = *table; ptr != (function_symrec *) 0; ptr = (function_symrec *)ptr->next){
        if (strcmp (ptr->name,ident) == 0){
            return ptr;
        }
    }
    return 0;
}

//Check to see if a function is in the function symbol table (for undefined
//function table)
//Returns 0 if not found. Returns 1 if found.
int function_in_table(char * ident, function_symrec** table){
    function_symrec* record = get_function_symbol(ident, table);
    if(record == ((function_symrec*) 0)){
        return 0;
    }

    return 1;
}

void update_function_parameter_count(char* ident, int parameter_count,function_symrec** table){
    function_symrec* ptr = get_function_symbol(ident, table);
    ptr->number_of_parameters = parameter_count;
}

//Print the function symbol table to stderr
void print_function_symbol_table(function_symrec** table){
    function_symrec *ptr;
    fprintf(stderr,"===Function symbol table===\n");
    for (ptr = *table; ptr != (function_symrec *) 0; ptr = (function_symrec *)ptr->next){
        fprintf(stderr, "%s(%d)   ->   #%d\n",(ptr)->name,(ptr)->number_of_parameters,(ptr)->address);
    }
}

void print_undefined_function_symbol_table(undefined_function_symrec** table){
    undefined_function_symrec *ptr;
    fprintf(stderr,"===Undefined function symbol table===\n");
    for (ptr = *table; ptr != (undefined_function_symrec *) 0; ptr = (undefined_function_symrec *)ptr->next){
        fprintf(stderr, "%s(%d) @ #%d.    [Patched status: %d]\n", ptr->name, ptr->number_of_parameters,ptr->address,ptr->has_been_patched);
    }
}
