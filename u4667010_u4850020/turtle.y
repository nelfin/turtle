/*
 * turtle.y
 *
 * COMP6361 assignment 2
 * u4667010
 * u4850020
 */
%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>

#include "turtle.tab.h"
#include "symtab.h"
#include "instructions.h"

extern int yylex();
extern FILE *yyin;
extern unsigned long lineno;

void yyerror(const char *s);
void turtle_log(const char *fmt, ...);
char *make_error_string(char *error_start, char *error_argument);
void add_instruction(short instruction);
void add_instruction_two_parts(short instruction, short address);
void patch_instruction(int instruction_index, short instruction);
void add_loadi_instruction(int n);
variable_symrec* get_variable_symbol_safe(char* ident,
                                          variable_symrec** table);
function_symrec* get_function_symbol_safe(char* ident,
                                          function_symrec** table);
short get_variable_symbol_address(char* ident, variable_symrec** table);
void add_global_to_symbol_table(char* identifier, variable_symrec** table);
void add_local_to_symbol_table(char* identifier, variable_symrec** table);
void initialise_global_variable(char* identifier, variable_symrec** table,
                                int value);
void initialise_local_variable(char* identifier, variable_symrec** table,
                               int value);
void add_function_to_symbol_table(char* identifier, int parameter_count,
                                  function_symrec** table);

//---Output---
#define COMPILE_LOG_ENABLED 0

//---Instruction recording---
#define MAX_PROGRAM_LENGTH 0xffff
short *instructions;
short instruction_output_index = 0;

//We need to jump over all of the functions, so we go straight from global
//declarations in the code to the start of the body. For this we record the
//instruction that we need to patch.
short function_skip_address;

//If we are currently parsing a function, this global variable is set to 1.
//Otherwise it's 0. Used to report errors on rogue return statements
int currently_parsing_function = false;

//The symbol tables
variable_symrec* variable_symbol_table = (variable_symrec *) 0;
function_symrec* function_symbol_table = (function_symrec *) 0;
undefined_function_symrec* undefined_function_symbol_table = (undefined_function_symrec *) 0;

//Count how many global and local variables we've come across
//(local_variable_counter gets reset for each function)
short global_variable_counter = 1;
short local_variable_counter = 1;

// We need to count how many parameters a function has
int function_parameter_counter = 1;

%}

%union {
    char *string;
    int token;
    int address;
    int list_count;
}

%token <string> TINT TIDENTIFIER
/* Reserved words */
%token <token>  TTURTLE TVAR TFUN TUP TDOWN TMOVETO TREAD TIF TELSE TWHILE
%token <token>  TRETURN
/* Operators */
%token <token>  TPLUS TMINUS TMUL TASSIGN TLPAREN TRPAREN TLBRACE TRBRACE
%token <token>  TCOMMA TLESS TEQUAL

/* Associativity and precedence */
%left TMINUS TPLUS
%left TMUL

/* Types */
%type <address> comparison
/*
 * We return the comparison backtrack patch address via the grammar element
 */
%type <address> conditional
/*
 * We return the else block patch address via the grammar element
 */

/* For counting parsed function parameters */
%type <list_count> expr_list
%type <list_count> expr_tail
%type <list_count> arg_list

/* Memory */
%start program

%%

program:    header global_decls body
        ;

header:     TTURTLE TIDENTIFIER
            {
                turtle_log("new turtle file (%s)\n", $2);
                free($2); //free the value the lexer strdup'd us
            }
        ;

global_decls:
            { turtle_log("---new declarations block---\n"); }
            global_var_decls
            {
                //Skip execution of the function code. That would be silly.
                add_instruction(JUMP_INST);
                function_skip_address = instruction_output_index;
                add_instruction(0);
                //we're going to come back to this one. We'll know where to
                //patch - to our variable function_skip_address

                currently_parsing_function = true;
                //we're about to start the function block
            }
            func_decls
            {
                currently_parsing_function = true;
                //we're done parsing functions
            }
        ;

body:
            {
                turtle_log("---body delcaration block---\n");
                //We now know where to jump to - here.
                patch_instruction(function_skip_address, instruction_output_index);
                //we're actually patching the jump location, so we set the
                //instruction jump destination to the current instruction
                //output index
            }
            block
        ;

local_var_decls:
            local_var_decls local_var_decl
        |   /* empty */
        ;
local_var_decl:   TVAR TIDENTIFIER TASSIGN expr
            {
                turtle_log("new local variable (%s)", $2);
                add_local_to_symbol_table($2, &variable_symbol_table);

                free($2); //free the value the lexer strdup'd us
            }
        |   TVAR TIDENTIFIER
            {
                turtle_log("new local variable (%s)\n", $2);
                initialise_local_variable($2, &variable_symbol_table,0);

                free($2); //free the value the lexer strdup'd us
            }
        ;
global_var_decls:
            global_var_decls global_var_decl
        |   /* empty */
        ;

global_var_decl:   TVAR TIDENTIFIER TASSIGN expr
            {
                turtle_log("new global variable (%s)\n", $2);
                add_global_to_symbol_table($2, &variable_symbol_table);
                //it just gets pushed to the stack. No need to store
                //explicitly. Same goes for other similar ones

                free($2); //free the value the lexer strdup'd us
            }
        |   TVAR TIDENTIFIER
            {
                turtle_log("new global variable (%s)\n", $2);
                initialise_global_variable($2, &variable_symbol_table, 0);

                free($2); //free the value the lexer strdup'd us
            }
        ;

func_decls:
            func_decls func_decl
        |   /* empty */
        ;

func_decl:
            TFUN TIDENTIFIER
            {
                turtle_log("_new function (%s)_\n", $2);
                add_function_to_symbol_table($2,0,&function_symbol_table);
                //function_parameter_counter is put in the table rather than 0
                //later (after parsing the block)

                //This is the point that JSR will arrive at. (after parsing
                //the function name, before parsing the parameter list)
                $<address>$ = instruction_output_index;
                //record the place to jump to, in case we're going to update a
                //function call to this place
            }
            param_list
            {
                //parses the parameters, and add instructions that store stack
                //values in the symbol table as local variables. At this
                //point, they're all argnum, as we don't know what n is yet
                //(pattern is arg#x -> x+(-n-1))

                //If this function was called before being defined, we want to
                //patch where we promised we would (go through the
                //undefined_function symbol table)
                undefined_function_symrec *ptr;
                for (ptr = undefined_function_symbol_table; ptr != (undefined_function_symrec *) 0; ptr = (undefined_function_symrec *)ptr->next){
                    if(strcmp (ptr->name,$2) == 0){
                        //$2 is the function identifier
                        //Check for wrong number of parameters error now,
                        //since we couldn't do it when calling
                        if((function_parameter_counter-1) != ptr->number_of_parameters){
                            //function_parameter_counter is a global variable
                            //which keeps track of parsed # of params (should
                            //really use the better way with list_counter, but
                            //this was done beforehand)
                            //Error message. We don't use yyerror, as we would
                            //like to report our own prerecorded line number
                            fprintf(
                                stderr,
                                "[Parse error!] (line %lu): "
                                "Function %s() expected %d parameters, "
                                "found %d\n",
                                ptr->actual_line_number,
                                ptr->name,
                                function_parameter_counter,
                                ptr->number_of_parameters);
                            exit(1);
                        } else {
                            //we have the right number of parameters, continue
                            patch_instruction(ptr->address,$<address>3);
                            //$<address>3 is the instruction we said we'd jump
                            //to (before param list was parsed) (set above as
                            //$<address>$)
                            ptr->has_been_patched = 1;
                            //we've patched it now, so record this as done
                        }
                    }
                }

                //Go through the symbol table and update the local variables
                //that are here (the only local variables at this point will
                //be the ones we just added while parsing the parameters)
                offset_local_variables(&variable_symbol_table,-function_parameter_counter-1);
                //x+(-n-1)
                turtle_log("Offset local variables\n");
                //Record the parameter count in the symbol table
                turtle_log("Recording parameter count %d for %s\n",(function_parameter_counter-1),$2);
                update_function_parameter_count($2, (function_parameter_counter-1), &function_symbol_table);
                //off by one with counter vs. count (0 args, counter at 1)

                //Reset the parameter counter for the next function
                function_parameter_counter = 1;

                free($2); //free the value the lexer strdup'd us
            }
            local_var_decls
            {
                local_variable_counter = 1;
                //Reset the local variable counter for next time
            }
            block
            {
                turtle_log("Parsed body\n");
                //DEBUG: before purging, print out the local symbol table, for
                //interest
                //print_variable_symbol_table(&variable_symbol_table);

                //We finished executing the function. Purge the local
                //variables in the symbol table
                purge_local_variables(&variable_symbol_table);
                turtle_log("Purged local variables\n");

                //Add a return statement for when we've finished executing
                //this function
                add_instruction(RTS_INST);
            }
        ;

param_list:
            TLPAREN ident_list TRPAREN
            { turtle_log("Parsed the parameter list\n"); }
        ;

ident_list:
            /* empty */
        |   TIDENTIFIER
            {
                turtle_log("Parsed parameter %s\n",$1);
                put_variable_symbol($1,function_parameter_counter,LOCAL,&variable_symbol_table);
                //offset when we know what it is (after counting)
                function_parameter_counter++;

                free($1); //free the value the lexer strdup'd us
            }
            ident_tail
            /*
             * As we parse the parameters in the function identifier list, we
             * record how many there are
             */
        ;

ident_tail:
            TCOMMA TIDENTIFIER
            {
                turtle_log("Parsed parameter %s\n",$2);
                put_variable_symbol($2,function_parameter_counter,LOCAL,&variable_symbol_table);
                //offset when we know what it is (after counting)
                function_parameter_counter++;

                free($2); //free the value the lexer strdup'd us
            }
            ident_tail /* same as above */
        |   /* empty */
        ;

up:
            TUP
            {
                turtle_log("<up> operation\n");
                add_instruction(UP_INST);
            }
        ;

down:
            TDOWN

            {
                turtle_log("<down> operation\n");
                add_instruction(DOWN_INST);
            }
        ;

moveto:
            TMOVETO TLPAREN expr TCOMMA expr TRPAREN
            {
                turtle_log("<moveto> operation\n");
                add_instruction(MOVE_INST);
                //we expect expr and expr to already be on the stack by the
                //time we get here
            }
        ;

read:
            TREAD TLPAREN TIDENTIFIER TRPAREN
            {

                //Get the address and type from the symbol table record
                variable_symrec* variable_symbol_record = get_variable_symbol_safe($3,&variable_symbol_table);
                short variable_address = variable_symbol_record->address;
                symbol_type variable_type = variable_symbol_record->type;

                //Store to this address. Store to different places if we're
                //talking about a local or global variable
                if(variable_type == GLOBAL){
                    add_instruction_two_parts(GLOBAL_READ_INST, variable_address);
                }else if(variable_type == LOCAL){
                    add_instruction_two_parts(FRAME_READ_INST, variable_address);
                }

                free($3); //free the value the lexer strdup'd us
            }
        ;

assignment:
            TIDENTIFIER TASSIGN expr
            {
                turtle_log("assignment (%s)=\n",$1);

                //Get the address and type from the symbol table record
                variable_symrec* variable_symbol_record = get_variable_symbol_safe($1,&variable_symbol_table);
                short variable_address = variable_symbol_record->address;
                symbol_type variable_type = variable_symbol_record->type;

                //Store to this address. Store to different places if we're
                //talking about a local or global variable
                if(variable_type == GLOBAL){
                    add_instruction_two_parts(GLOBAL_STORE_INST, variable_address);
                }else if(variable_type == LOCAL){
                    add_instruction_two_parts(FRAME_STORE_INST, variable_address);
                }

                free($1); //free the value the lexer strdup'd us

            }
        ;

comparison:
            TLPAREN expr TLESS expr TRPAREN
            {
                //Compare and test
                add_instruction(SUB_INST);
                add_instruction(TEST_INST);

                add_instruction(POP_INST); //we're done with the subbed value
                add_instruction(1); //pop 1

                //Code parsed immediately after comparison should be the
                //address to jump to if comparison succeeds. This is an
                //incomplete two word instruction
                add_instruction(JLT_INST);

            }
        |   TLPAREN expr TEQUAL expr TRPAREN
            {
                //Compare and test
                add_instruction(SUB_INST);
                add_instruction(TEST_INST);

                add_instruction(POP_INST); //we're done with the subbed value
                add_instruction(1); //pop 1

                //Code parsed immediately after comparison should be the
                //address to jump to if comparison succeeds. This is an
                //incomplete two word instruction
                add_instruction(JEQ_INST);
            }
        ;

conditional:
            TIF comparison
            {
                add_instruction(instruction_output_index+1+2);
                //jump over the skip to the else block (what we're about to
                //add in the next two instructions) if comparison succeeds.

                //The skip to the else block
                add_instruction(JUMP_INST);
                $<address>$ = instruction_output_index;
                //we pass the instruction index via the grammar attribute for
                //this mid-rule action. $<address>$ is used temporarily
                add_instruction(0);
                //we're going to patch this with the correct address later.
            }
            stmt
            {
                //Skip over the else clause if comparison succeeded. Insert
                //placeholder
                add_instruction(JUMP_INST);
                $<address>$ = instruction_output_index;
                //we pass the instruction index via the grammar attribute (as
                //above)
                add_instruction(0);

                //we now know where to jump to if the comparison fails, so
                //update the address we were going to.
                //We will arrive at this point if the comparison failed (so
                //execute else clause)
                patch_instruction($<address>3,instruction_output_index);
                //$<address>3 references the semantic value from the first mid
                //rule action
            }
            elseclause
            {
                patch_instruction($<address>5,instruction_output_index);
                //we access the semantic value for the previous mid rule
                //action. (as above) It's #5 since we count symbols AND
                //actions (also as above)
            }
        ;

elseclause:
            /* empty */
        |   TELSE stmt
        ;

whileloop:
            TWHILE
            {
                //Save the location of the start of the while loop
                $<address>$ = instruction_output_index;
                //we pass the instruction index via the grammar attribute
            }
            comparison
            {
                //Immediately after comparison, we specify the address that
                //we're going to jump to if the comparison succeeded
                //So, skip the next two instructions
                add_instruction(instruction_output_index+1+2);
                //jump over the skip to the else block (what we're about to
                //add in the next two instructions) if comparison succeeds.

                //Now, if we're executing this part, the comparison failed. In
                //that case, we want to exit the while loop
                add_instruction(JUMP_INST);
                $<address>$ = instruction_output_index;
                //we pass the instruction index via the grammar attribute
                add_instruction(0); //patch this later with $<address>4
            }
            stmt
            {
                //Go back to the start of the while loop to re-check condition
                //(we can only exit when comparison fails)
                add_instruction(JUMP_INST);
                add_instruction($<address>2);

                //This is the loop exit point
                patch_instruction($<address>4,instruction_output_index);
                //we now know where the loop exit is
            }
        ;

return:
            TRETURN expr
            {
                //If we're not in a function, error
                if (!currently_parsing_function)
                    yyerror("Return statement outside of function");

                //If we reach this point in execution, we have an early return
                //statement to execute
                turtle_log("Return statement\n");
                //Assume that the top of the function symbol table is the
                //current function
                int return_value_offset = -function_symbol_table->number_of_parameters - 2;
                //Expr is on the top of the stack, store it in the correct
                //location
                add_instruction_two_parts(FRAME_STORE_INST, return_value_offset);
                add_instruction(RTS_INST);
            }
        ;

call:
            TIDENTIFIER
            {
                //Load zero on the stack to reserve a location for the result
                //of the function
                add_loadi_instruction(0);
            }
            arg_list
            {
                turtle_log("Call to function (%s)\n",$1);
                //First, check if the function has been defined yet. If it
                //hasn't, we might need to come back to it and patch it
                if(!function_in_table($1,&function_symbol_table)){
                    //It is yet to be defined (presumably). Insert a
                    //placeholder jump address, and patch later
                    add_instruction(JSR_INST);
                    int instruction_to_patch = instruction_output_index;
                    //record it
                    add_instruction(0); //we'll patch it

                    //Collect the values to store in the table
                    long unsigned int actual_line_number = lineno;
                    int called_parameter_count = $3;

                    put_undefined_function(
                        $1, instruction_to_patch, actual_line_number,
                        called_parameter_count,
                        &undefined_function_symbol_table);

                    //Add the rest of the instructions for calling
                    add_instruction(POP_INST);
                    add_instruction(called_parameter_count);
                    //doesn't matter that this =/= necessarily the defined
                    //count, as if they're different, we won't be running
                    //anyway
                } else {
                    //It has been defined already, so get the data from the
                    //table for adding the right instructions
                    function_symrec* symbol_table_entry = get_function_symbol_safe($1, &function_symbol_table);
                    short function_address = symbol_table_entry->address;
                    add_instruction(JSR_INST);
                    //jump to the subroutine, with address...
                    add_instruction(function_address);
                    //check to make sure we're calling with the right number
                    //of parameters
                    int symbol_table_parameter_count = symbol_table_entry->number_of_parameters;
                    int called_parameter_count = $3;
                    //we recursively parse arg_list and keep track of the
                    //argument count in the semantic value

                    if(symbol_table_parameter_count != called_parameter_count){
                        char * error_string = malloc(sizeof(char) * (100 + 10 + 10 + strlen($1)));
                        //reasonable length for digits is 10, right? That's a
                        //big number of parameters
                        // (That's more than 0x7fff parameters, pretty sure
                        // the language doesn't support that...)
                        sprintf(
                            error_string,
                            "Function %s() expected %d parameters, found %d",
                            $1,
                            symbol_table_parameter_count,
                            called_parameter_count);
                        yyerror(error_string);
                        //We don't free the error_string, but it's an error
                        //message, so it'll die anyway
                    }

                    //when we're done with calling, we'll have the parameters
                    //on the stack that we need to pop
                    add_instruction(POP_INST); //Pop...
                    add_instruction(symbol_table_parameter_count);// ...n
                }

                //and the return value afterwards

                free($1);
                //free the value the lexer strdup'd us
            }
        ;

arg_list:
            TLPAREN expr_list TRPAREN
            {
                $$ = $2; //the count for arg_list is that of expr_list
            }
        ;

expr_list:
            /* empty */
        {
            $$ = 0; //base case
        }
        |   expr expr_tail
        {
            $$ = $2 + 1; //add one to count, parse tail of list to add more
        }
        ;

expr_tail:
        TCOMMA expr expr_tail
        {
            $$ = $3 + 1; //add one to count, parse tail of list to add more
        }
        |   /* empty */
        {
            $$ = 0; //base case
        }
        ;

stmt:
            up | down | moveto | read | assignment | conditional | whileloop
        |   return
        |   call
            {
                //If we call in a statement, pop the return value, as we don't
                //use it
                add_instruction(POP_INST); //Pop...
                add_instruction(1);// ...1
            }
        | block
        ;

stmt_list:
            stmt_list stmt
        |   /* empty */
        ;

block:
            TLBRACE stmt_list TRBRACE
        ;

expr:
            TINT
            {
                add_loadi_instruction(atoi($1));
                free($1);
            }
        |   TIDENTIFIER
            {
                //Get the address and type from the symbol table record
                variable_symrec* variable_symbol_record = get_variable_symbol_safe($1,&variable_symbol_table);
                short variable_address = variable_symbol_record->address;
                symbol_type variable_type = variable_symbol_record->type;

                //Load from this address, onto the top of the stack. Load from
                //different places if we're talking about a local or global
                //variable
                if(variable_type == GLOBAL){
                    add_instruction_two_parts(GLOBAL_LOAD_INST, variable_address);
                } else { //local
                    add_instruction_two_parts(FRAME_LOAD_INST, variable_address);
                }

                free($1); //free the value the lexer strdup'd us

            }
        | call
        | expr TPLUS expr {add_instruction(ADD_INST);}
        | expr TMINUS expr {add_instruction(SUB_INST);}
        | expr TMUL expr {add_instruction(MUL_INST);}
        | TMINUS expr {add_instruction(NEG_INST);}
        | TLPAREN expr TRPAREN
        ;

%%

//We won't know until the end of the function declaration block whether there
//are undefined functions. We do it at the very end, as it makes little
//difference when we check
void undefined_function_check(){
    undefined_function_symrec *ptr;
    for (ptr = undefined_function_symbol_table; ptr != (undefined_function_symrec *) 0; ptr = (undefined_function_symrec *)ptr->next){
        if(!(ptr->has_been_patched)){
            //Error message. We don't use yyerror, as we would like to report
            //our own prerecorded line number
            fprintf(stderr, "[Parse error] (line %lu): Undefined function: %s()\n",ptr->actual_line_number,ptr->name);
            exit(1);
        }
    }
}

int main() {
    //Initialise the instruction array
    instructions = malloc(sizeof(short) * MAX_PROGRAM_LENGTH);

    do {
        yyparse();
    } while (!feof(yyin));

    turtle_log("Writing program to stdout\n");
    //Print out the instructions when done
    int i;
    for(i = 0; i < instruction_output_index; i++){
        fprintf(stdout,"%d\n", instructions[i]);
    }

    undefined_function_check();

    //Also print the symbol tables for debugging if log is enabled
    if(COMPILE_LOG_ENABLED){
        print_variable_symbol_table(&variable_symbol_table);
        print_function_symbol_table(&function_symbol_table);
        print_undefined_function_symbol_table(&undefined_function_symbol_table);
    }

    return 0;
}

/*******************************
 *  Instruction table methods  *
 *******************************/
void add_instruction(short instruction){
    instructions[instruction_output_index] = instruction;
    instruction_output_index++;
}

//LOAD, STORE and READ instructions, second part is a two's complement
//address. This function adds a two part instruction in one word (with two's
//complement)
void add_instruction_two_parts(short instruction, short address){
    if(address < 0){
        address += 256;
        // Two's complement? It seems to work
    }
    add_instruction(instruction + address);
}

//Sometimes we need to backtrack and patch instructions. Given the index, and
//the instruction, set the instruction to output at the index
void patch_instruction(int instruction_index, short instruction){
    instructions[instruction_index] = instruction;
}

//Adds two instructions to load n to the stack
void add_loadi_instruction(int n){
    add_instruction(LOADI_INST);
    add_instruction(n);
}

/****************************************
 *  Retrieving symbols from the symtab  *
 ****************************************/
//A wrapper for get_variable_symbol, that reports errors.
//get_variable_symbol() should *only* be used in this function in this file,
//use the safe version elsewhere
variable_symrec* get_variable_symbol_safe(char* ident, variable_symrec**
table){
    variable_symrec* record = get_variable_symbol(ident, table);
    if(record == (variable_symrec*) 0){
        yyerror(make_error_string("Undefined variable",ident));
    }
    return record;
}

//A wrapper for get_function_symbol, that reports errors.
//get_function_symbol() should *only* be used in this function in this file,
//use the safe version elsewhere
function_symrec* get_function_symbol_safe(char* ident, function_symrec**
table){
    function_symrec* record = get_function_symbol(ident, table);
    if(record == (function_symrec*) 0){
        yyerror(make_error_string("Undefined function",ident));
    }
    return record;
}

//Returns the stored address of a symbol from the given table
short get_variable_symbol_address(char* ident, variable_symrec** table){
    short address = get_variable_symbol_safe(ident,table)->address;
    return address;
}

/**********************************
 *  Adding symbols to the symtab  *
 **********************************/
//Adding to symbol table (locally)
void add_global_to_symbol_table(char* identifier, variable_symrec** table) {
    put_variable_symbol(identifier,global_variable_counter,GLOBAL,table);
    global_variable_counter++;
}

//Adding to the symbol table (globally)
void add_local_to_symbol_table(char* identifier, variable_symrec** table) {
    put_variable_symbol(identifier,local_variable_counter,LOCAL,table);
    local_variable_counter++;
}

//Initiailising variables (make instructions and store symbol location)
//globally
void initialise_global_variable(char* identifier, variable_symrec** table, int value){
    add_global_to_symbol_table(identifier, table); //Record the location for future reference
    add_loadi_instruction(value); //push value to the stack
}

//locally
void initialise_local_variable(char* identifier, variable_symrec** table, int
value){
    add_loadi_instruction(value); //push value to the stack
    add_local_to_symbol_table(identifier, table); //Record the location for future reference
}

//Adds a function to the symbol table (wraps around put_function_symbol, but
//also records the address of the function so we can jump to it)
void add_function_to_symbol_table(char* identifier, int parameter_count,
function_symrec** table){
    put_function_symbol(identifier,parameter_count,instruction_output_index,table);
}

/*********************
 *  Error reporting  *
 *********************/
void yyerror(const char *s) {
    fprintf(stderr, "[Parse error] (line %lu): %s\n", lineno, s);
    exit(1);
}

void turtle_log(const char *fmt, ...) {
    if(COMPILE_LOG_ENABLED){
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
    }
}

//Join two strings with a ": " in between them
char* make_error_string(char* error_start, char* error_argument){
    char* joining_string = ": ";
    int total_error_length = (strlen(error_start)+strlen(joining_string)+strlen(error_argument));
    char* error_message = malloc(sizeof(char) * total_error_length);
    strcpy(error_message,error_start);
    strcat(error_message,joining_string);
    strcat(error_message,error_argument);
    return error_message;
}

/* vim: set ft=yacc.c : */

