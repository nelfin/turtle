/*
 * symtab.h
 *
 * Header file for the symbol table
 * Heavily modified from the bison & flex lab code
 *
 * COMP6361 assignment 2
 * u4667010
 * u4850020
 */
#ifndef SYMTAB_H
#define SYMTAB_H

typedef enum {LOCAL, GLOBAL} symbol_type;

struct variable_symrec {
    char *name;
    short address;
    symbol_type type;
    struct variable_symrec *next;
};

struct function_symrec {
    char *name;
    short address;
    int number_of_parameters;
    struct function_symrec *next;
};

//A table to keep track of functions that have been called, but are yet to be delcared
struct undefined_function_symrec {
    char *name;
    short address;
    long unsigned int actual_line_number;
    int number_of_parameters;
    int has_been_patched; //if nonzero, the function has since been defined. Otherwise, it is yet to be defined (so that we don't have to delete from the table)
    struct undefined_function_symrec *next;
};

typedef struct variable_symrec variable_symrec;
typedef struct function_symrec function_symrec;
typedef struct undefined_function_symrec undefined_function_symrec;

undefined_function_symrec *put_undefined_function(char* ident, short location,int actual_line_number, int number_of_parameters,undefined_function_symrec** table);

variable_symrec *put_variable_symbol(char* ident,short location,symbol_type symbol,variable_symrec** table);
variable_symrec *get_variable_symbol(char* ident,variable_symrec** table);
variable_symrec* purge_local_variables(variable_symrec** table);
variable_symrec* offset_local_variables(variable_symrec** table, int offset);

function_symrec *put_function_symbol(char* ident, int parameter_count, short location,function_symrec** table);
function_symrec *get_function_symbol(char* ident,function_symrec** table);
int function_in_table(char* ident, function_symrec** table);
void update_function_parameter_count(char* ident, int parameter_count,function_symrec** table);

void print_variable_symbol_table(variable_symrec** table);
void print_function_symbol_table(function_symrec** table);
void print_undefined_function_symbol_table(undefined_function_symrec** table);

#endif /* end of include guard: SYMTAB_H */
