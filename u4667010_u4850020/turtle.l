/*
 * turtle.l
 *
 * COMP6361 assignment 2
 * u4667010
 * u4850020
 */
%{
#include <stdio.h>

#include "turtle.tab.h"

#define ERR_MSG_LEN 100
char err_msg[ERR_MSG_LEN];
extern void yyerror(const char *s);

#define SAVE_TOKEN  (yylval.string = strdup(yytext))
#define TOKEN(t)    (yylval.token = t)
#define ERROR(f,s)  \
    snprintf(err_msg, ERR_MSG_LEN, (f), (s)); \
    yyerror(err_msg);

unsigned long lineno = 1;
%}

DIGIT   [0-9]
LETTER  [a-zA-Z]
CHAR    [a-zA-Z0-9_']
ID      {LETTER}{CHAR}*

%%
"//"[^\n]*              ; /* ignore comments */
[[:blank:]\r]           ; /* ignore whitespace */
{DIGIT}+                SAVE_TOKEN; return TINT;
turtle                  return TOKEN(TTURTLE);
var                     return TOKEN(TVAR);
fun                     return TOKEN(TFUN);
up                      return TOKEN(TUP);
down                    return TOKEN(TDOWN);
moveto                  return TOKEN(TMOVETO);
read                    return TOKEN(TREAD);
if                      return TOKEN(TIF);
else                    return TOKEN(TELSE);
while                   return TOKEN(TWHILE);
return                  return TOKEN(TRETURN);
{ID}                    SAVE_TOKEN; return TIDENTIFIER;
"--"                    ; /* pre-optimising double negatives */
"+"                     return TOKEN(TPLUS);
"-"                     return TOKEN(TMINUS);
"*"                     return TOKEN(TMUL);
"="                     return TOKEN(TASSIGN);
"("                     return TOKEN(TLPAREN);
")"                     return TOKEN(TRPAREN);
"{"                     return TOKEN(TLBRACE);
"}"                     return TOKEN(TRBRACE);
","                     return TOKEN(TCOMMA);
"<"                     return TOKEN(TLESS);
"=="                    return TOKEN(TEQUAL);
\n                      ++lineno;
.                       ERROR("unrecognized character `%s`", yytext)

%%


/* vim: set ft=lex.c : */

