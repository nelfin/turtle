/*
 * instructions.h
 *
 * Instruction opcodes
 *
 * COMP6361 assignment 2
 * u4667010
 * u4850020
 */
#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#define UP_INST             0x0A00
#define DOWN_INST           0x0C00
#define MOVE_INST           0x0E00
#define LOADI_INST          0x5600
#define GLOBAL_STORE_INST   0x0400 //04XX, where X is index
#define FRAME_STORE_INST    0x0500 //05XX
#define GLOBAL_LOAD_INST    0x0600 //06XX
#define FRAME_LOAD_INST     0x0700 //07XX
#define JSR_INST            0x6800
#define RTS_INST            0x2800
#define JUMP_INST           0x7000
#define POP_INST            0x5E00
#define ADD_INST            0x1000
#define SUB_INST            0x1200
#define NEG_INST            0x2200
#define MUL_INST            0x1400
#define TEST_INST           0x1600
#define JLT_INST            0x7400
#define JEQ_INST            0x7200
#define GLOBAL_READ_INST    0x0200 //02XX, where X is index
#define FRAME_READ_INST     0x0300 //03XX

#endif /* end of include guard: INSTRUCTIONS_H */
