turtle sierpinski

//Draws a basic sierpinski triangle, the fun way! (chaos game with pixels)

var currentX
var currentY
var nextVertex
var iterations
var iteration

//Functions
fun div ( dividend , divisor )
var quotient = 0
{
    while ( divisor < dividend ) {
        quotient = quotient + 1
        dividend = dividend - divisor
    }
    if ( dividend + dividend < divisor ) {
        return quotient
    }
    else {
        return quotient + 1
    }
}


fun moveHalfwayTo(x,y)
{
    currentX = div(x+currentX,2)
    currentY = div(y+currentY,2)
}

fun dot(){
    moveto(currentX-1,currentY)
    down
    moveto(currentX,currentY)
    up
}

fun abs(x){
    if(x < 0){
        return -x
    }else{
        return x
    }
}


//Main
{
    read(iterations)
    while(iteration < iterations){
        read(nextVertex) //gets the next vertex
        
        if(nextVertex== 0){
            moveHalfwayTo(0,0)
        }
        
        if(nextVertex==1){
            moveHalfwayTo(500,0)
        }
        
        if(nextVertex==2){
            moveHalfwayTo(250,433)
        }
        
        dot()
        
        iteration = iteration + 1
    }

}

