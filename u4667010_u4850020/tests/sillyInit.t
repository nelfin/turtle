//Test by Timothy Cosgrove and Jarrah Bloomfield
turtle Twinkle
// Draw a few stars

//Very silly way to initialise some global variables
//Works since functions may be called before they are declared
var posX = return500 ()
var posY = return500 ()
var size = return25 ()

fun return500 () {
   return 500
}
fun return25 () {
   return 25
}

// Draw a single star
fun star (x, y, scale)
  var topY = y +8*scale
{
  up
  moveto (x, y)
  down
  moveto (x+3*scale, y+6*scale)
  moveto (x-3*scale, y+6*scale)
  moveto (x, y)
  up
  moveto (x, topY)
  down
  moveto (x+3*scale, y+2*scale)
  moveto (x-3*scale, y+2*scale)
  moveto (x, topY)
  up
}
// Main body of program
{
  while (0 < size) {
    star (posX, posY, size)
    posX = posX - 80
    posY = posY - 4*size
    size = size - 4
  }  
}



