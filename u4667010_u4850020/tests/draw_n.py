#Writes a file draw_n.d for draw_n.t to read. Makes an n-gon

from math import *

out = open("draw_n.d", "w");

n = int(raw_input("degree of ngon? "));

RADIUS = 212
X = 310
Y = 350

out.write(str(n+1))
out.write("\n")

for i in range(1, n):
   out.write( str(int(floor(sin(i*2*pi/n )*RADIUS)) + X))
   out.write("\n")
   out.write( str(int(floor(cos(i*2*pi/n)*RADIUS)) + Y))
   out.write("\n")

out.write( str(int(floor(sin(2*pi)*RADIUS)) + X))
out.write("\n")
out.write( str(int(floor(cos(2*pi)*RADIUS)) + Y))
out.write("\n")
out.write( str(int(floor(sin(2*pi/n )*RADIUS)) + X))
out.write("\n")
out.write( str(int(floor(cos(2*pi/n)*RADIUS)) + Y))
