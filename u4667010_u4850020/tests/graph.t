turtle graph

//A basic test of if statements and while loops

var iteration = 0

fun plot()
var xScale = 25
var x
var y
{
    x = iteration * xScale
    y = iteration * iteration * 3 + 100
    
    if (300 < y) {
        y = 300
    }
    
    moveto(x,y)
    
    iteration = iteration + 1
}



{
    up
    down
    while(iteration < 100){
        plot()
    }
}
