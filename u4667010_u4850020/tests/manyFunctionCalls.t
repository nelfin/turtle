//Test by Timothy Cosgrove and Jarrah Bloomfield
turtle ohmaigoodness
// makes an AMAZINGLY excessive amount of function calls.
// The PDPlot simulator may exceed its memory limit if return values
// are not correctly popped from the stack.
var x
var y
var n = 10000

fun dotup() {
up
return 8+9
}

fun dotdown() {
down
return 1+1+1
}

{
while (0 < n){
dotdown()
x = x + 1
y = y + 2
moveto(x, y)
dotup()
x = x + 1
y = y + 2
moveto(x, y)
n = n - 1
}}
