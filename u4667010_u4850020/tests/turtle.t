//Test by Timothy Cosgrove and Jarrah Bloomfield
turtle Turtle
//ultimate stress test. Draws a very stressed-looking turtle,
//after stressing the pdPLOT simulator for a little over 5 seconds.
//This file also has some irregular whitespace hiding somewhere which
//may crash some lexers
var currentx = 0
var currenty = 0
var circn
var circx
var circy
//was supposed to draw a circle
//but due to problems approximating square roots, it's now a diamond
fun circle(radius, x, y)
   var nx = x - radius
   var ny = y
{
   up
   moveto(nx,ny)
   down
   
   //top semicircle
   while (nx < x+radius)
   {
      nx = nx+1
      ny = y
      while(inrange(radius,nx,ny,x,y) == 0)
      {
         ny = ny + 1
      }
      moveto(nx,ny)
   }
   
   nx = x + radius
   ny = y
   moveto(nx,ny)
   
   //bottom semicircle
   while (x-radius < nx)
   {
      nx = nx-1
      ny = y
      while(inrange(radius,nx,ny,x,y) == 0)
      {
         ny = ny - 1
      }
      moveto(nx,ny)
   }
   nx = x - radius
   ny = y
   moveto(nx,ny)
   up
   
}
fun inrange(radius,nx,ny,x,y)
   var xdiff = abs(x-nx)
   var ydiff = abs(y-ny)
{
   //if (radius < isqrt(ydiff*ydiff + xdiff*xdiff))
   if (radius < ydiff+xdiff)
    {
      return 1
   }
   return 0
}
fun abs(num)
{
   if (num < 0)
   {
      return -num
   }
   return num
}

fun isqrt(number)
   var res = 0
   var bit = 2*2*2*2*2*2*2*2*2*2*2*2*2*2
{
   while (number < bit)
   {
      bit = div(bit, 4)
   }
   
   while (0 < bit)
   {
      if (res + bit < number)
     {
        number = number - res - bit
        res = div(res, 2) + bit
     } else 
	 { 
	    if (res + bit == number)
        {
           number = number - res - bit
           res = div(res, 2) + bit
        } else
        {
          res = div(res, 2)
        }
	 }
     bit = div(bit,2)
   }
   
   return res
}
fun div (dividend, divisor)
  var quotient = 0
{

  while (divisor < dividend) {
    quotient = quotient + 1
    dividend = dividend - divisor
  }
  return quotient
}
fun drawlegs(radiusdiv4, x, y, widthdiv2, height)
	var nx = x
	var ny = y
{   
   //first two legs
   
   nx = x - radiusdiv4 - widthdiv2
   drawlineavoidcircle(4*radiusdiv4, x, y, nx, ny, height)
   nx = x - radiusdiv4 + widthdiv2
   drawlineavoidcircle(4*radiusdiv4, x, y, nx, ny, height)
   
   //2nd two legs
   
   nx = x + radiusdiv4 - widthdiv2
   drawlineavoidcircle(4*radiusdiv4, x, y, nx, ny, height)
   nx = x + radiusdiv4 + widthdiv2
   drawlineavoidcircle(4*radiusdiv4, x, y, nx, ny, height)
}
fun drawlineavoidcircle(radius, x, y, nx, ny, height)
{
   ny = y - height
   up
   moveto(nx,ny)
   down
   while (inrange(radius,nx,ny,x,y) == 1)
   {
      ny = ny + 1
   }
   moveto(nx,ny)
   
   up
   while (inrange(radius,nx,ny,x,y) == 0)
   {
      ny = ny + 1
   }
   moveto(nx,ny)
   
   down
   ny = y+height
   moveto(nx,ny)
   up
}
//Draws the main body circle from input file 
// (taken from a draw_n.t ngon)
fun body()
{
up
read (circn)
read(circx)
read(circy)
moveto(circx,circy)
circn = circn - 1
down
while (0 < circn) 
{
read(circx)
read(circy)
moveto(circx,circy)
circn = circn - 1
}
}
{
   //main diamond
   circle(300, 310, 350)
   //draw 4 legs
   drawlegs(75, 310, 350, 25, 300)
   //draw feet
   circle(25, 310-75, 350+300)
   circle(25, 310+75, 350+300)
   circle(25, 310-75, 350-300)
   circle(25, 310+75, 350-300)
   circle(50, 310, 350+300+50)
   //draw main circle
   body()
   //draw eyes
   circle(5, 310-15, 350+300+50+20)
   circle(5, 310+15, 350+300+50+20)
}
