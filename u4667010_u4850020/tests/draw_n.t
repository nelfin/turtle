//Test by Timothy Cosgrove and Jarrah Bloomfield
turtle little
var n
var x
var y
// Reads a number n from draw_n.d then n conencts n x,y pairs.
// Currently draws an n-gon generated in python by draw_n.py
{
up
read (n)
read(x)
read(y)
moveto(x,y)
n = n - 1
down
while (0 < n) 
{
read(x)
read(y)
moveto(x,y)
n = n - 1
}
}
