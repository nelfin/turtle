turtle flower

//Reads parameters from the .d file, and makes a beautiful field of flowers (note: beauty depends on flower parameters)
//First parameter in the .d file is the number of flowers to draw. A bunch of 0's separate each flower (just discarded by the program, makes editing individual flowers visually easier though)

var FLOWER_HEIGHT = 100 //height from base to stem, in pixels
var FLOWER_X = 200 //x coordinate of base of stem
var FLOWER_Y = 0 //y coordinate of base of stem
var FLOWER_RADIUS = 15 //radius of the middle part of the flower
var PETAL_LENGTH = 45 //length of each petal
var STEM_STRAIGHTNESS = 500 //stem droop factor. 1 gives line y=x^2, higher it is, closer it is to straight line. Reasonable range 200-1500

var ROUNDED_CORNERS_QUALITY = 20 //not influenced by .d file. Samples ROUNDED_CORNERS_QUALITY points along each circle's width to draw it

var flowerMiddleX
var flowerMiddleY
var flowersToDraw
var flowerNumber

var junkVariable //used to read "separator" numbers from flower.d



//---MATHS FUNCTIONS---

fun abs(x){
    if(x < 0){
        return -x
    }else{
        return x
    }
}

fun div ( dividend , divisor ) //can deal with negative divisors only
var quotient = 0
var sign = 1
{
    if(divisor < 0){
        divisor = abs(divisor)
        sign = -1
    }
    while ( divisor < dividend ) {
        quotient = quotient + 1
        dividend = dividend - divisor
    }
    if ( dividend + dividend < divisor ) {
        return quotient
    }
    else {
        return quotient + 1
    }
}



fun sqrt(n)
var oldguess = -1
var guess = 1
{
    while(1 < abs(guess-oldguess)){
        oldguess = guess
        guess = div((guess + div(n,guess)),2)
    }
    
    return guess
}

fun square(n) { return n*n}

//It's a bad circle, but I don't have trig, so cut me some slack ok
fun drawCircle(x,y, radius)
var decrement = div(radius * 2, ROUNDED_CORNERS_QUALITY)
var plotX = radius
var plotY
{
    up
    moveto(x+radius,y)
    down
    //Top semi circle
    while (-radius < plotX){
        plotX = plotX - decrement
        plotY = sqrt(radius * radius - plotX * plotX)
        moveto(x+plotX,y+plotY)
    }
    //Bottom semi circle
    while (plotX < radius){
        plotX = plotX + decrement
        plotY = -sqrt(radius * radius - plotX * plotX)
        moveto(x+plotX,y+plotY)
    }
    up
}

//---DRAWING FUNCTIONS---

fun drawStem(baseX, baseY, height)
var iteration = 0
var xShift //for making the stem droop a bit
{
    up
    moveto(baseX, baseY)
    down    
    
    while (iteration < height){
        iteration = iteration + 5
        xShift = iteration * iteration
        xShift = div(xShift,STEM_STRAIGHTNESS)
        moveto(baseX+xShift, baseY + iteration)
        
    }
    
    up
    return baseX + div(height * height,STEM_STRAIGHTNESS)
}

fun drawRect(x1,x2,y1,y2){
    up
    moveto(x1,y1)
    down
    moveto(x2,y1)
    moveto(x2,y2)
    moveto(x1,y2)
    moveto(x1,y1)
    up
}

fun drawLine(x1,x2,y1,y2){
    up
    moveto(x1,y1)
    down
    moveto(x2,y2)
    up
}

fun drawVerticalSemiCircle(xStart,xEnd,y,direction) //direction = {1,-1}
var x = xStart
var yShift
var radius = div(xEnd-xStart,2)
var width = xEnd-xStart
var step = div(width,ROUNDED_CORNERS_QUALITY)
var relativeX //our position relative to the start
{
    up
    moveto(xStart,y)
    down
    
    while(x < xEnd){
        x = x + step
        relativeX = x - xStart
        
        //Semi circle
        yShift = sqrt(square(radius) - square(relativeX-radius)) * direction
        moveto(x,y+yShift)
    }
    up
}


fun drawHorizontalSemiCircle(x,yStart,yEnd,direction) //direction = {1,-1}
var y = yStart
var xShift
var radius = div(yEnd-yStart,2) //Goes up
var width = yEnd-yStart
var step = div(width,ROUNDED_CORNERS_QUALITY)
var relativeY //our position relative to the start
{
    up
    moveto(x,yStart)
    down
    
    while(y < yEnd){
        y = y + step
        relativeY = y - yStart
        
        //Semi circle
        xShift = sqrt(square(radius) - square(relativeY-radius)) * direction
        moveto(x+xShift,y)
    }
    up
}


{
    read(flowersToDraw)
    
    //Read flower data from the file (overwriting the global "constants")
    while(flowerNumber < flowersToDraw){
        up
        read(FLOWER_HEIGHT)
        read(FLOWER_X)
        read(FLOWER_Y)
        read(FLOWER_RADIUS)
        read(PETAL_LENGTH)
        read(STEM_STRAIGHTNESS)
        read(junkVariable) //discard one read value, as this read value will separate flower data in the input (enabling simpler modification)
        //-----Begin drawing the flower-----
        
        flowerMiddleX = drawStem(FLOWER_X,FLOWER_Y,FLOWER_HEIGHT)
        flowerMiddleY = FLOWER_HEIGHT + FLOWER_RADIUS
        drawCircle(flowerMiddleX,flowerMiddleY,FLOWER_RADIUS) //shift up to not overlap with the stem
        
        //Draw the straight petal lines
        
        //Vert lines
        drawLine(flowerMiddleX - FLOWER_RADIUS,flowerMiddleX - FLOWER_RADIUS,flowerMiddleY - PETAL_LENGTH,flowerMiddleY + PETAL_LENGTH) //long vertical left line
        drawLine(flowerMiddleX + FLOWER_RADIUS,flowerMiddleX + FLOWER_RADIUS,flowerMiddleY - PETAL_LENGTH,flowerMiddleY + PETAL_LENGTH) //long vertical right line
        
        //Horizontal lines
        drawLine(flowerMiddleX - PETAL_LENGTH,flowerMiddleX - 1 *FLOWER_RADIUS,flowerMiddleY - FLOWER_RADIUS,flowerMiddleY - FLOWER_RADIUS) //horizontal top line (left)
        drawLine(flowerMiddleX - PETAL_LENGTH,flowerMiddleX - 1 *FLOWER_RADIUS,flowerMiddleY + FLOWER_RADIUS,flowerMiddleY + FLOWER_RADIUS) //horizontal bot line (left)
        
        drawLine(flowerMiddleX + 1 *FLOWER_RADIUS,flowerMiddleX + PETAL_LENGTH,flowerMiddleY - FLOWER_RADIUS,flowerMiddleY - FLOWER_RADIUS) //horizontal top line (right)
        drawLine(flowerMiddleX + 1 *FLOWER_RADIUS,flowerMiddleX + PETAL_LENGTH,flowerMiddleY + FLOWER_RADIUS,flowerMiddleY + FLOWER_RADIUS) //horizontal bot line (right)
        
        //Draw the rounded ends of the petals
        drawVerticalSemiCircle(flowerMiddleX - FLOWER_RADIUS,flowerMiddleX + FLOWER_RADIUS,flowerMiddleY + PETAL_LENGTH,1)
        drawVerticalSemiCircle(flowerMiddleX - FLOWER_RADIUS,flowerMiddleX + FLOWER_RADIUS,flowerMiddleY - PETAL_LENGTH,-1)
        drawHorizontalSemiCircle(flowerMiddleX - PETAL_LENGTH, flowerMiddleY - FLOWER_RADIUS, flowerMiddleY + FLOWER_RADIUS,-1)
        drawHorizontalSemiCircle(flowerMiddleX + PETAL_LENGTH, flowerMiddleY - FLOWER_RADIUS, flowerMiddleY + FLOWER_RADIUS,1)
        
        //Draw grass
        drawLine(FLOWER_X,FLOWER_X-10,FLOWER_Y,FLOWER_Y+25)
        drawLine(FLOWER_X,FLOWER_X+15,FLOWER_Y,FLOWER_Y+40)
        drawLine(FLOWER_X,FLOWER_X+30,FLOWER_Y,FLOWER_Y+25)
        
        //-----Done drawing the flower-----
        
        flowerNumber = flowerNumber + 1
        
    }
    
}
