//Test by Timothy Cosgrove and Jarrah Bloomfield
turtle ifelse
// If/else block demonstration
// Reads needforcheese and needforbacon from ifelse.d.
// Gives greatest desired food first.
// Note: If needs are equal, gives bacon first (obvious superiority)
var needforcheese
var needforbacon
var globX = 100
var globY = 0
fun drawbacon(width, height)
{
   up
   moveto (globX, globY)
   down
   moveto(globX + width, globY + height)
   moveto(globX + 2*width, globY)
   moveto(globX + 3*width, globY + height)
   moveto(globX + 4*width, globY)
   moveto(globX + 5*width, globY + height)
   moveto(globX + 5*width, globY + 3*height)
   moveto(globX + 4*width, globY + 2*height)
   moveto(globX + 3*width, globY + 3*height)
   moveto(globX + 2*width, globY + 2*height)
   moveto(globX + 1*width, globY + 3*height)
   moveto(globX, globY + 2*height)
   moveto(globX, globY)
   up
   globY = globY + 5*height
}
fun drawcheese(width, height)
{
   up
   moveto (globX, globY)
   down
   moveto (globX+width, globY+height)
   moveto (globX, globY+2*height)
   moveto (globX, globY)
   up
   globY = globY + 2*height
}
//decide what to draw
{
   read(needforcheese)
   read(needforbacon)
   
   if (needforcheese < needforbacon)
   {
      //draw bacon higher than cheese
      globY = globY + 100
      drawcheese(200, 50)
      globY = globY + 100
      drawbacon(40, 33)
   } else {
      //draw cheese higher than bacon
      globY = globY + 100
      drawbacon(40, 33)
      globY = globY + 100
      drawcheese(200, 50)
   }
}
